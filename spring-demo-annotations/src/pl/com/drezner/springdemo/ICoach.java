package pl.com.drezner.springdemo;

public interface ICoach {

	public String getDailyWorkout();
	
	public String getDailyFortune();
}

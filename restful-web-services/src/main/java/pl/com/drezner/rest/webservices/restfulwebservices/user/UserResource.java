package pl.com.drezner.rest.webservices.restfulwebservices.user;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import pl.com.drezner.rest.webservices.restfulwebservices.post.Post;
import pl.com.drezner.rest.webservices.restfulwebservices.post.PostDaoService;

@RestController
public class UserResource {

	@Autowired
	private UserDaoService userService;
	
	@Autowired
	private PostDaoService postService;

	@GetMapping("/users")
	public List<User> retrieveAllUsers() {
		return this.userService.findAll();
	}

	@GetMapping("/users/{id}")
	public Resource<User> retrieveUser(@PathVariable int id) {
		User user = this.userService.findOne(id);

		if (user == null) {
			throw new UserNotFoundException("id-" + id);
		}

		Resource<User> resource = new Resource<User>(user);
		
		ControllerLinkBuilder linkTo = 
				ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(this.getClass()).retrieveAllUsers());
		
		resource.add(linkTo.withRel("all-users"));
		
		
		return resource;
	}
	
	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable int id) {
		User user = this.userService.deleteById(id);

		if (user == null) {
			throw new UserNotFoundException("id-" + id);
		}
	}

	@PostMapping("/users")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
		User savedUser = this.userService.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}
	
	@GetMapping("/users/{id}/posts")
	public List<Post> retrieveAllUserPosts(@PathVariable int id) {
		User user = this.userService.findOne(id);
		
		return user.getPosts();
	}
	
	@GetMapping("/users/{id}/posts/{post_id}")
	public Post retrieveUserPost(@PathVariable("id") int id, @PathVariable("post_id") int postId) {
		return this.userService.findUserPost(id, postId);
	}
	
	@PostMapping("/users/{id}/posts")
	public ResponseEntity<Object> createUserPosts(@PathVariable int id, @RequestBody Post post) {
		Post savedPost = this.postService.save(post);
		
		this.userService.saveUserPost(id, savedPost);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{post_id}").buildAndExpand(savedPost.getId())
				.toUri();
		
		return ResponseEntity.created(location).build();
	}
}

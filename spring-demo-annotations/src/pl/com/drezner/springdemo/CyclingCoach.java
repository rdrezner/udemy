package pl.com.drezner.springdemo;

public class CyclingCoach implements ICoach {

	private FortuneService fortuneService;
	
	public CyclingCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Ride 400 km per week";
	}

	@Override
	public String getDailyFortune() {
		return this.fortuneService.getFortune();
	}

}

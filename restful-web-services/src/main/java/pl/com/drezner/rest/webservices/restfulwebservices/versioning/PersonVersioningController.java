package pl.com.drezner.rest.webservices.restfulwebservices.versioning;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonVersioningController {

	@GetMapping("v1/person")
	public PersonV1 personV1() {
		return new PersonV1("John Kowalsky");
	}

	@GetMapping("v2/person")
	public PersonV2 personV2() {
		return new PersonV2(new Name("Adam", "Nowak"));
	}

	@GetMapping(value = "/person/param", params = "version=1")
	public PersonV1 paramV1() {
		return new PersonV1("John Kowalsky");
	}

	@GetMapping(value = "/person/param", params = "version=2")
	public PersonV2 paramV2() {
		return new PersonV2(new Name("Adam", "Nowak"));
	}

	@GetMapping(value = "/person/header", headers = "X-Api-Version=1")
	public PersonV1 headerV1() {
		return new PersonV1("John Kowalsky");
	}

	@GetMapping(value = "/person/header", headers = "X-Api-Version=2")
	public PersonV2 headerV2() {
		return new PersonV2(new Name("Adam", "Nowak"));
	}
	
	@GetMapping(value = "/person/produces", produces = "application/pl.com.drezner.udemy-v1+json")
	public PersonV1 producesV1() {
		return new PersonV1("John Kowalsky");
	}

	@GetMapping(value = "/person/produces", produces = "application/pl.com.drezner.udemy-v2+json")
	public PersonV2 producesV2() {
		return new PersonV2(new Name("Adam", "Nowak"));
	}
}

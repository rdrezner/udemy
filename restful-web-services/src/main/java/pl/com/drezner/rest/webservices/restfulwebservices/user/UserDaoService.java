package pl.com.drezner.rest.webservices.restfulwebservices.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import pl.com.drezner.rest.webservices.restfulwebservices.post.Post;

@Component
public class UserDaoService {

	private static List<User> users = new ArrayList<>();

	private static int usersCount = 3;

	static {
		users.add(new User(1, "Adam", new Date()));
		users.add(new User(2, "Eve", new Date()));
		users.add(new User(3, "Jack", new Date()));
	}

	public List<User> findAll() {
		return users;
	}

	public User save(User user) {
		if (user.getId() == null) {
			user.setId(++usersCount);
		}
		users.add(user);
		return user;
	}

	public User findOne(int id) {
		for (User user : users) {
			if (user.getId() == id) {
				return user;
			}
		}
		return null;
	}

	public User deleteById(int id) {
		Iterator<User> iterator = users.iterator();
		while (iterator.hasNext()) {
			User user = iterator.next();
			if (user.getId() == id) {
				iterator.remove();
				return user;
			}
		}
		return null;
	}

	public void saveUserPost(int id, Post post) {
		User user = findOne(id);
		if (user != null) {
			List<Post> posts = user.getPosts();
			if (posts == null) {
				posts = new ArrayList<Post>();
				user.setPosts(posts);
			}
			posts.add(post);
		}
	}

	public Post findUserPost(int id, int postId) {
		User user = findOne(id);
		if (user != null) {
			List<Post> posts = user.getPosts();
			if (posts != null) {
				for (Post post : posts) {
					if (post.getId() == postId) {
						return post;
					}
				}
			}
		}
		return null;
	}
}

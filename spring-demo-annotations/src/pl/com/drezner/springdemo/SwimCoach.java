package pl.com.drezner.springdemo;

import org.springframework.beans.factory.annotation.Value;

public class SwimCoach implements ICoach {

	@Value("${foo.email}")
	private String email;
	
	@Value("${foo.team}")
	private String team;

	private FortuneService fortuneService;
	
	public SwimCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}
	
	public String getEmail() {
		return email;
	}

	public String getTeam() {
		return team;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Swim for 1 hour";
	}

	@Override
	public String getDailyFortune() {
		return this.fortuneService.getFortune();
	}

}

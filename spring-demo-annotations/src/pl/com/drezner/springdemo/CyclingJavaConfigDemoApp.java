package pl.com.drezner.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CyclingJavaConfigDemoApp {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);

		ICoach myCoach = context.getBean("cyclingCoach", ICoach.class);

		System.out.println(myCoach.getDailyWorkout());
		System.out.println(myCoach.getDailyFortune());

		context.close();
	}

}

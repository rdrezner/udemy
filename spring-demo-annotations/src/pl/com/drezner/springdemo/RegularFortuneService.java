package pl.com.drezner.springdemo;

public class RegularFortuneService implements FortuneService {

	@Override
	public String getFortune() {
		return "Today is pretty normal day";
	}

}

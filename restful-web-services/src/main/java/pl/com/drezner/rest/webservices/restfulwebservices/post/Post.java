package pl.com.drezner.rest.webservices.restfulwebservices.post;

public class Post {
	private Integer id;
	private String body;

	public Post() {
		super();
	}

	public Post(Integer id, String body) {
		super();
		this.id = id;
		this.body = body;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return String.format("Post [id=%s, body=%s]", id, body);
	}

}

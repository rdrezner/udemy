package pl.com.drezner.rest.webservices.restfulwebservices.post;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class PostDaoService {
	private static List<Post> posts = new ArrayList<>();

	private static int postsCount = 0;


	public Post save(Post post) {
		if (post.getId() == null) {
			post.setId(++postsCount);
		}
		posts.add(post);
		return post;
	}
}
